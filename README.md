# SVGo

High performance SVG Library for Golang.

## Usage

Create a new Image (in this case 300 by 300).
```golang
img = svgo.Image{}
img.Init(300, 300)
```

Then you can draw shapes on it. In this example we are drawing 2 lines of different colors.
```golang
img.Line(130, 70, 20, 130, 10, "#8447f1")
img.Line(50, 50, 230, 150, 10, "#5a85ee")
```
![two_lines](examples/two_line_first.svg)

The last object drawn will always be on top, so if we revert the two lines like so:
```golang
img.Line(50, 50, 230, 150, 10, "#5a85ee")
img.Line(130, 70, 20, 130, 10, "#8447f1")
```
The first line will be below the second line.  
![two_lines](examples/two_line_second.svg)

You can also add circles or rectangles.
```golang
img.Rect(50, 50, 100, 150, "#f98c19", "", 0)
img.Circle(80, 70, 20, "#c34d13", "", 0)
img.Line(50, 50, 230, 150, 10, "#5a85ee")
img.Line(130, 70, 20, 130, 10, "#8447f1")
```
![two_lines](examples/combined.svg)

To render the image you can either render it to a String:
```golang
str := img.Render()
```
Or directly render it to a file:
```golang
img.RenderToFile("example.svg")
```

## Example

This code:
```golang
package svgo

import (
	"fmt"
	"math/rand"
	"time"
)

func main() {
	rand.Seed(time.Now().UTC().UnixNano())

	img := svgo.Image{}
	img.Init(300, 300)

	for i := 1; i <= 10; i++ {
		img.Rect(rn(width), rn(height), rn(width), rn(height), rc(), "", 0)
	}

	for i := 1; i <= 15; i++ {
		img.Circle(rn(width), rn(height), rn(30), rc(), "", 0)
	}

	for i := 1; i <= 20; i++ {
		img.Line(rn(width), rn(height), rn(width), rn(height), rn(5), rc())
	}

	img.RenderToFile("random.svg")
}

func rn(max int) int {
	return rand.Intn(max-1) + 1
}
func rc() string {
	c := rn(16777215)
	return "#" + fmt.Sprintf("%x", c)
}
```

Will render an image like this:  
![random](examples/random.svg)