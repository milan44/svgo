package svgo

import "bytes"

// PathBuilder is used to combine multiple paths
type PathBuilder struct {
	path                bytes.Buffer
	attributes          string
	hasContents         bool
	duplicationChecking bool
}

// Reset resets the PathBuilder to its initial state
func (pb *PathBuilder) Reset(attributes string) {
	pb.path = bytes.Buffer{}
	pb.path.WriteString(`<path d="`)
	pb.attributes = attributes
	pb.hasContents = false
}

// Add adds a path to the PathBuilder
func (pb *PathBuilder) Add(path string) {
	if pb.duplicationChecking {
		bPath := []byte(path)
		if bytes.Contains(pb.path.Bytes(), bPath) {
			return
		}
	}
	pb.path.WriteString(path)
	pb.hasContents = true
}

// String combines all paths and their attributes and constructs a path tag
func (pb *PathBuilder) String() string {
	if !pb.hasContents {
		return ""
	}

	pb.path.WriteString(`" `)
	pb.path.WriteString(pb.attributes)
	pb.path.WriteString(" />")

	return pb.path.String()
}
