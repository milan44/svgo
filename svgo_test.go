package svgo

import (
	"fmt"
	"io/ioutil"
	"math/rand"
	"os"
	"testing"
	"time"
)

var files = []string{}

func TestSVGo(t *testing.T) {
	os.Mkdir("tests", 0777)

	rectangleTest()
	circleTest()
	ellipseTest()
	lineTest()

	randomTest()
	readMeExamples()

	style := `style="border: 1px solid #000000;"`

	readme := "<h1>Tests</h1><table style=\"border-collapse: collapse\"><tr><th " + style + ">Description</th><th " + style + ">Image</th></tr>"
	for i := 0; i < len(files); i += 2 {
		readme += "<tr><th " + style + ">" + files[i] + "</th><td " + style + "><img style=\"width: 200px;height: 200px;\" src=\"../" + files[i+1] + "\" /></td></tr>"
	}
	ioutil.WriteFile("tests/README.html", []byte(readme+"</table>"), 0777)
}

func readMeExamples() {
	img := Image{}
	img.Init(300, 300)

	img.Line(130, 70, 20, 130, 10, "#8447f1")
	img.Line(50, 50, 230, 150, 10, "#5a85ee")

	img.RenderToFile("examples/two_line_first.svg")

	img = Image{}
	img.Init(300, 300)

	img.Line(50, 50, 230, 150, 10, "#5a85ee")
	img.Line(130, 70, 20, 130, 10, "#8447f1")

	img.RenderToFile("examples/two_line_second.svg")

	img = Image{}
	img.Init(300, 300)

	img.Rect(50, 50, 100, 150, "#f98c19", "", 0)
	img.Circle(80, 70, 20, "#c34d13", "", 0)
	img.Line(50, 50, 230, 150, 10, "#5a85ee")
	img.Line(130, 70, 20, 130, 10, "#8447f1")

	img.RenderToFile("examples/combined.svg")
}

func rectangleTest() {
	// One rectangle
	img := Image{}
	img.Init(100, 100)

	img.Rect(10, 10, 60, 50, "#5a85ee", "", 0)

	files = append(files, "One rectangle", "tests/one_rect.svg")
	img.RenderToFile(files[len(files)-1])

	// Two different rectangles
	img = Image{}
	img.Init(100, 100)

	img.Rect(10, 10, 60, 50, "#5a85ee", "", 0)
	img.Rect(20, 30, 90, 90, "#8447f1", "", 0)

	files = append(files, "Two different rectangles", "tests/two_rect_diff.svg")
	img.RenderToFile(files[len(files)-1])

	// Two rectangles
	img = Image{}
	img.Init(100, 100)

	img.Rect(10, 10, 60, 50, "#5a85ee", "", 0)
	img.Rect(20, 30, 90, 90, "#5a85ee", "", 0)

	files = append(files, "Two same rectangles", "tests/two_rect.svg")
	img.RenderToFile(files[len(files)-1])
}
func circleTest() {
	// One circle
	img := Image{}
	img.Init(100, 100)

	img.Circle(40, 40, 35, "#5a85ee", "", 0)

	files = append(files, "One circle", "tests/one_circle.svg")
	img.RenderToFile(files[len(files)-1])

	// Two different circles
	img = Image{}
	img.Init(100, 100)

	img.Circle(40, 40, 35, "#5a85ee", "", 0)
	img.Circle(60, 60, 35, "#8447f1", "", 0)

	files = append(files, "Two different circles", "tests/two_circle_diff.svg")
	img.RenderToFile(files[len(files)-1])

	// Two circles
	img = Image{}
	img.Init(100, 100)

	img.Circle(40, 40, 35, "#5a85ee", "", 0)
	img.Circle(60, 60, 35, "#5a85ee", "", 0)

	files = append(files, "Two same circles", "tests/two_circle.svg")
	img.RenderToFile(files[len(files)-1])
}
func ellipseTest() {
	// One ellipse
	img := Image{}
	img.Init(100, 100)

	img.Ellipse(50, 50, 40, 10, "#5a85ee", "", 0)

	files = append(files, "One ellipse", "tests/one_ellipse.svg")
	img.RenderToFile(files[len(files)-1])

	// Two different ellipses
	img = Image{}
	img.Init(100, 100)

	img.Ellipse(50, 50, 40, 10, "#5a85ee", "", 0)
	img.Ellipse(50, 50, 10, 40, "#8447f1", "", 0)

	files = append(files, "Two different ellipses", "tests/two_ellipse_diff.svg")
	img.RenderToFile(files[len(files)-1])

	// Two ellipses
	img = Image{}
	img.Init(100, 100)

	img.Ellipse(50, 50, 40, 10, "#5a85ee", "", 0)
	img.Ellipse(50, 50, 10, 40, "#5a85ee", "", 0)

	files = append(files, "Two same ellipses", "tests/two_ellipse.svg")
	img.RenderToFile(files[len(files)-1])
}
func lineTest() {
	// One line
	img := Image{}
	img.Init(100, 100)

	img.Line(10, 10, 90, 90, 4, "#5a85ee")

	files = append(files, "One line", "tests/one_line.svg")
	img.RenderToFile(files[len(files)-1])

	// Two different lines
	img = Image{}
	img.Init(100, 100)

	img.Line(10, 10, 90, 90, 4, "#5a85ee")
	img.Line(10, 90, 90, 10, 4, "#8447f1")

	files = append(files, "Two different lines", "tests/two_line_diff.svg")
	img.RenderToFile(files[len(files)-1])

	// Two lines
	img = Image{}
	img.Init(100, 100)

	img.Line(10, 10, 90, 90, 4, "#5a85ee")
	img.Line(10, 90, 90, 10, 4, "#5a85ee")

	files = append(files, "Two same lines", "tests/two_line.svg")
	img.RenderToFile(files[len(files)-1])
}
func randomTest() {
	rand.Seed(time.Now().UTC().UnixNano())

	width := 100
	height := 100

	img := Image{}
	img.Init(width, height)

	for i := 1; i <= 2; i++ {
		img.Rect(rn(width), rn(height), rn(width), rn(height), rc(), "", 0)
	}

	for i := 1; i <= 3; i++ {
		img.Circle(rn(width), rn(height), rn(30), rc(), "", 0)
	}

	for i := 1; i <= 5; i++ {
		img.Line(rn(width), rn(height), rn(width), rn(height), rn(4), rc())
	}

	files = append(files, "Random image", "tests/random.svg")
	img.RenderToFile(files[len(files)-1])
}

func rn(max int) int {
	return rand.Intn(max-1) + 1
}
func rc() string {
	c := rn(16777215)
	return "#" + fmt.Sprintf("%x", c)
}
