package svgo

import (
	"bytes"
	"strconv"
)

// Object is an object like a rectangle, line, circle, etc.
type Object struct {
	path       string
	attributes string
	tag        string
}

// NewObject creates a new Object given a path and attributes
func NewObject(tag string, path string, attributes string) Object {
	return Object{
		path:       path,
		attributes: attributes,
		tag:        tag,
	}
}

// Attributes creates an attributes string based on fill, stroke and stroke width
func Attributes(fill string, stroke string, strokeWidth int) string {
	attr := bytes.Buffer{}
	space := ""
	if fill != "" {
		writeToBytesBuffer(&attr, `fill="`, fill, `"`)
		space = " "
	}
	if stroke != "" {
		writeToBytesBuffer(&attr, space, `stroke="`, stroke, `"`)
		space = " "
	}
	if strokeWidth != 0 {
		writeToBytesBuffer(&attr, space, `stroke-width="`, strconv.Itoa(strokeWidth), `"`)
	}
	return attr.String()
}

// Rect creates the rectangle path
func Rect(x int, y int, width int, height int, attributes string) Object {
	path := _concat("M", strconv.Itoa(x), ",", strconv.Itoa(y), "l", strconv.Itoa(width), ",0l0,", strconv.Itoa(height), "l", strconv.Itoa(-width), ",0Z")

	return NewObject("p", path, attributes)
}

// Line creates the line path
func Line(x1 int, y1 int, x2 int, y2 int, attributes string) Object {
	path := _concat("M", strconv.Itoa(x1), " ", strconv.Itoa(y1), " L", strconv.Itoa(x2), " ", strconv.Itoa(y2))

	return NewObject("p", path, attributes)
}

// Circle creates the circle path
func Circle(x int, y int, radius int, attributes string) Object {
	xMinusRadius := x - radius
	doubleRadius := radius * 2
	doubleRadiusMinus := radius * -2

	strRadius := strconv.Itoa(radius)

	path := _concat("M", strconv.Itoa(xMinusRadius), ",", strconv.Itoa(y), " a", strRadius, ",", strRadius, " 0 1,0 ", strconv.Itoa(doubleRadius), ",0 a", strRadius, ",", strRadius, " 0 1,0 ", strconv.Itoa(doubleRadiusMinus), ",0")

	return NewObject("p", path, attributes)
}

// Ellipse creates the ellipse path
func Ellipse(x int, y int, radiusX int, radiusY int, attributes string) Object {
	xMinusRadiusX := strconv.Itoa(x - radiusX)
	doubleRadiusX := strconv.Itoa(radiusX * 2)
	doubleRadiusXMinus := strconv.Itoa(radiusX * -2)
	strRadiusX := strconv.Itoa(radiusX)
	strRadiusY := strconv.Itoa(radiusY)

	path := _concat("M", xMinusRadiusX, ",", strconv.Itoa(y), "a", strRadiusX, ",", strRadiusY, " 0 1,0 ", doubleRadiusX, ",0a", strRadiusX, ",", strRadiusY, " 0 1,0 ", doubleRadiusXMinus, ",0")

	return NewObject("p", path, attributes)
}

func _concat(strs ...string) string {
	buf := bytes.Buffer{}
	for _, s := range strs {
		buf.WriteString(s)
	}
	return buf.String()
}
