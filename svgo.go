package svgo

import (
	"bytes"
	"io/ioutil"
	"strconv"
	"sync"
)

// Image is the canvas which will be used to draw on
type Image struct {
	source  bytes.Buffer
	objects []Object
	mutex   sync.Mutex

	customSource bytes.Buffer
}

func writeToBytesBuffer(buff *bytes.Buffer, strs ...string) {
	for _, s := range strs {
		buff.WriteString(s)
	}
}

// Reset deletes all drawn paths
func (img *Image) Reset() {
	img.objects = make([]Object, 0)
}

// AddObject adds an Object to the Image
func (img *Image) AddObject(obj Object) {
	img.mutex.Lock()
	img.objects = append(img.objects, obj)
	img.mutex.Unlock()
}

// AddCustomSource adds custom source to the Image
func (img *Image) AddCustomSource(src string) {
	img.mutex.Lock()
	img.customSource.WriteString(src)
	img.mutex.Unlock()
}

// Init initializes the Image
func (img *Image) Init(width int, height int) {
	img.objects = make([]Object, 0)
	img.source = bytes.Buffer{}
	img.mutex = sync.Mutex{}

	writeToBytesBuffer(&img.source, `<svg height="`, strconv.Itoa(width), `" width="`, strconv.Itoa(height), `" viewBox="0 0 `, strconv.Itoa(width), ` `, strconv.Itoa(height), `" xmlns="http://www.w3.org/2000/svg">`)
}

// Rect draws a rectangle
func (img *Image) Rect(x int, y int, width int, height int, fill string, stroke string, strokeWidth int) {
	img.AddObject(Rect(x, y, width, height, Attributes(fill, stroke, strokeWidth)))
}

// Line draws a line
func (img *Image) Line(x1 int, y1 int, x2 int, y2 int, width int, color string) {
	img.AddObject(Line(x1, y1, x2, y2, Attributes("", color, width)))
}

// Circle draws a circle
func (img *Image) Circle(x int, y int, radius int, fill string, stroke string, strokeWidth int) {
	img.AddObject(Circle(x, y, radius, Attributes(fill, stroke, strokeWidth)))
}

// Ellipse draws an Ellipse
func (img *Image) Ellipse(x int, y int, radiusX int, radiusY int, fill string, stroke string, strokeWidth int) {
	img.AddObject(Ellipse(x, y, radiusX, radiusY, Attributes(fill, stroke, strokeWidth)))
}

// Render renders the image to a String
func (img *Image) Render(dontDrawTwice bool) string {
	src := img.source
	objects := img.objects

	lastAttributes := ""

	pathBuilder := PathBuilder{duplicationChecking: dontDrawTwice}
	pathBuilder.Reset("")

	for _, obj := range objects {
		if obj.tag != "p" {
			src.WriteString(pathBuilder.String())
			pathBuilder.Reset(lastAttributes)
			src.WriteString(_concat("<", obj.tag, " ", obj.attributes, " />"))
		} else {
			if obj.attributes == lastAttributes {
				pathBuilder.Add(obj.path)
			} else {
				src.WriteString(pathBuilder.String())
				pathBuilder.Reset(obj.attributes)
				pathBuilder.Add(obj.path)
				lastAttributes = obj.attributes
			}
		}
	}
	src.WriteString(pathBuilder.String())
	src.WriteString(img.customSource.String())

	src.WriteString(`</svg>`)

	return src.String()
}

// RenderToFile renders the image to a file
func (img *Image) RenderToFile(file string, dontDrawTwice bool) {
	ioutil.WriteFile(file, []byte(img.Render(dontDrawTwice)), 0777)
}
